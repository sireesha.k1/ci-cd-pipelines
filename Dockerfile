FROM python:3.6-slim
ADD . /simplecicd
WORKDIR /simplecicd
CMD [ "python", "test.py" ]

